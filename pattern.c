#include <stdio.h>

int main()
{
    printf("Enter the nth value:\n");
    int n, c = 40;
    scanf("%d",&n);
    int s;
    
    for(int i = 0; i < n; i++){
        for(int j = 0; j < c/2-i; j++){
            printf(" ");
        }
        s = s*i;
        for(int k = s; k <= i ; k++){
            printf("*");
            printf(" ");
        }
        printf("\n");
    }
    return 0;
}